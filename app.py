from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/get-student")
async def get_student():
    return {
        'name' : 'Mama'
    }
    
@app.get("/get-class-room")
async def get_class_room():
    return {
        'class': 'math'
    }